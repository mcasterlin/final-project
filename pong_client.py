import socket
import sys
import thread
import threading
import json
import Tkinter as tk
import tkFont
import random
import winsound


host = 'localhost'
port = 50000
backlog = 5
size = 1024

parameters = {}
commands = {}

def run_client():
	global parameters,commands

	#init client
	client = None
	try:
		client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		client.sendto("hi!",(host,port))
	except socket.error, (value,message):
		if client:
			client.close()
		print "Could not open socket: " + message
		sys.exit(1)

	t_receiving = threading.Thread(target = receiving, args = (client,))
	t_receiving.start()

	while 1:
		# var = raw_input("-> ")
		# client.sendto(var,(host,port))
		after(5,sending(client,commands))
		# client.sendto(parameters,(host,port))

def sending(client,commands):
	global parameters, commands
	client.sendto(json.dumps(commands),(host,port))


def receiving(client):
	global parameters, commands
	while 1:
		parameters,_ = client.recvfrom(size)
		parameters = json.loads(parameters)
		print "Received: ", parameters, "\r"
		commands = app.commands
		app.draw()
 
class GUI(tk.Frame):
#---------CONTROLLER----------
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
 
        # Set up the grid space for the application
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.ourFont = tkFont.Font(family='fixedsys',size=20)
        self.ourFont2 = tkFont.Font(family='fixedsys',size=50)

        #self.pong = Pong()

        self.bindKeys(master)
        self.createWidgets()
        self.pack()

        self.field = tk.Canvas(master, width=1000, height=500, bg = "black")
        self.importGraphics()

        self.command_state = False
        self.commands = {"w":False,"s":False,"up":False,"down":False,"d":False,"a":False,"left":False,"right":False,"any":False,"Quit":False,"Restart":False}

        self.run()

    # keybindings to control the positioning of the paddles
    def bindKeys(self, master):
        """
        Binding all keys which control the game components.
        """
        #Paddle Controls
        master.bind("w", lambda event: self.set_key("w"))#self.pong.paddle_left.lift)
        master.bind("<KeyRelease-w>", lambda event: self.reset_key("w"))#self.pong.paddle_left.stop_left)
        master.bind("s", lambda event: self.set_key("s"))#self.pong.paddle_left.lower)
        master.bind("<KeyRelease-s>", lambda event: self.reset_key("s"))#self.pong.paddle_left.stop_left)
        master.bind("<Up>", lambda event: self.set_key("up"))#self.pong.paddle_right.lift)
        master.bind("<KeyRelease-Up>", lambda event: self.reset_key("up"))#self.pong.paddle_right.stop_right)
        master.bind("<Down>", lambda event: self.set_key("down"))#self.pong.paddle_right.lower)
        master.bind("<KeyRelease-Down>", lambda event: self.reset_key("down"))#self.pong.paddle_right.stop_right)
        master.bind("d", lambda event: self.set_key("d"))#lambda event: self.pong.enable_left_warp(0))
        master.bind("a", lambda event: self.set_key("a"))#lambda event: self.pong.enable_left_warp(1))
        master.bind("<KeyRelease-d>", lambda event: self.reset_key("d"))#self.pong.disable_left_warp)
        master.bind("<KeyRelease-a>", lambda event: self.reset_key("a"))#self.pong.disable_left_warp)
        master.bind("<Left>", lambda event: self.set_key("left"))#lambda event: self.pong.enable_right_warp(0))
        master.bind("<Right>", lambda event: self.set_key("right"))#lambda event: self.pong.enable_right_warp(1))
        master.bind("<KeyRelease-Left>", lambda event: self.reset_key("left"))#self.pong.disable_right_warp)
        master.bind("<KeyRelease-Right>", lambda event: self.reset_key("right"))#self.pong.disable_right_warp)
        master.bind("<Key>", lambda event: self.set_key("any"))#self.pong.begin_game)

    # creates buttons to exit the game or reset the field conditions
    def createWidgets(self):
        """
        Creates buttons which provide control for overall state of the application.
        """
        self.quit = tk.Button(self, text="Quit", command=lambda:self.set_key("Quit"), font = self.ourFont).grid(column=1,row=0)
        self.restart = tk.Button(self, text="New Game", command=lambda:self.set_key("Restart"), font = self.ourFont).grid(column=0,row=0, padx=335, sticky="E")

    def set_key(self, event):

        for key in self.commands:
            if key == event:
                self.commands[key] = True

    def reset_key(self, event):

        for key in self.commands:
            if key == event:
                self.commands[key] = False

    def run(self):
        print self.commands, "\n"
        root.after(5, self.run)
#-----------VIEW--------------
    # draw the updated boundaries of all relevant game elements (paddles, ball, etc) on the canvas game field
    def importGraphics(self):
        """
        Imports and resizes the images which represent the game components.
        """
        self.cube_img = tk.PhotoImage(file="comp_cube.gif")
        self.cube_img = self.cube_img.subsample(4,4)
        self.ball_img = tk.PhotoImage(file="space_core.gif")
        self.ball_img = self.ball_img.subsample(30,30)
        self.paddle_img = tk.PhotoImage(file="caution2.gif")
        self.paddle_img = self.paddle_img.subsample(4,4)
        self.bg_img = tk.PhotoImage(file="background.gif")
        self.creature_img = tk.PhotoImage(file="currupt_cube.gif")
        self.creature_img = self.creature_img.subsample(4,4)

    def draw(self):
        """
        Updates the visual representation of all game components when called.
        """
        global parameters

        app.field.delete("all")

        #Create the background image
        self.field.create_image(500,250,image=self.bg_img)

        #Displays a timer to convey information about ball delay after resets
        if parameters["restart_delay"] < 1000 and parameters["begin"] == True:
            timer_angle = -359*(parameters["restart_delay"]/1000.)
            self.field.create_arc(451,86,551,186, start = 90, extent = timer_angle, outline = "purple", style = "arc", width = 10)

        #Create paddles
        if parameters["lwarp_enable"] == True:
            p1_color = parameters["lwarp"]
        if parameters["rwarp_enable"] == True:
            p2_color = parameters["rwarp"]
        self.field.create_image(parameters["paddle_left_center"], image = self.paddle_img)
        self.field.create_image(parameters["paddle_right_center"], image = self.paddle_img)
        if parameters["lwarp_enable"] == True:
            self.field.create_oval(37, (parameters["paddle_left_height"]+10), 52, (parameters["paddle_left_height"]-110), fill = p1_color)
            self.field.create_oval(40, (parameters["paddle_left_height"]-5), 49, (parameters["paddle_left_height"]-95), fill = "gray11")
        if parameters["rwarp_enable"] == True:
            self.field.create_oval(948, (parameters["paddle_right_height"]+10), 963, (parameters["paddle_right_height"]-110), fill=p2_color)     
            self.field.create_oval(951, (parameters["paddle_right_height"]-5), 960, (parameters["paddle_right_height"]-95), fill="gray11")

        #Paddle Portals
        if parameters["cyan_portal_override"] == False:
            self.field.create_oval(parameters["orange_portal_space"][0][0]-13,parameters["orange_portal_space"][0][1]-13,parameters["orange_portal_space"][1][0]+13,parameters["orange_portal_space"][1][1]+13, fill = parameters["orange_portal_color_in"])
            self.field.create_oval(parameters["orange_portal_space"][0][0], parameters["orange_portal_space"][0][1], parameters["orange_portal_space"][1][0], parameters["orange_portal_space"][1][1], fill=parameters["orange_portal_fill_color"])
        if parameters["orange_portal_override"] == False:
            self.field.create_oval(parameters["cyan_portal_space"][0][0]-13,parameters["cyan_portal_space"][0][1]-13,parameters["cyan_portal_space"][1][0]+13,parameters["cyan_portal_space"][1][1]+13, fill = parameters["cyan_portal_color_in"])
            self.field.create_oval(parameters["cyan_portal_space"][0][0], parameters["cyan_portal_space"][0][1], parameters["cyan_portal_space"][1][0], parameters["cyan_portal_space"][1][1], fill=parameters["cyan_portal_fill_color"])
        if parameters["blue_portal.override"] == False:   
            self.field.create_oval(parameters["red_portal_space"][0][0]-13,parameters["red_portal_space"][0][1]-13,parameters["red_portal_space"][1][0]+13,parameters["red_portal_space"][1][1]+13, fill = parameters["red_portal_color_in"])
            self.field.create_oval(parameters["red_portal_space"][0][0], parameters["red_portal_space"][0][1], parameters["red_portal_space"][1][0], parameters["red_portal_space"][1][1], fill=parameters["red_portal_fill_color"])
        if parameters["red_portal.override"] == False:
            self.field.create_oval(parameters["blue_portal_space"][0][0]-13,parameters["blue_portal_space"][0][1]-13,parameters["blue_portal_space"][1][0]+13,parameters["blue_portal_space"][1][1]+13, fill = parameters["blue_portal_color_in"])
            self.field.create_oval(parameters["blue_portal_space"][0][0], parameters["blue_portal_space"][0][1], parameters["blue_portal_space"][1][0], parameters["blue_portal_space"][1][1], fill=parameters["blue_portal_fill_color"])
        
        #Create breakout block
        self.field.create_image(parameters["block1_center"][0],parameters["block1_center"][1], image = self.cube_img, state = parameters["block1_state"])        

        #Create creature
        self.field.create_image(parameters["creature_center"][0],parameters["creature_center"][1], image = self.creature_img, state = parameters["creature_state"])

        #Display score
        app.field.create_text(275, 38, text=str(parameters["scoreP1"]), fill = "dark green", font=self.ourFont)
        app.field.create_text(725, 38, text=str(parameters["scoreP2"]), fill = "dark green", font=self.ourFont)
        
        #Relay game state information to players
        if parameters["gravity_cooldown"] > 4500 and parameters["gravity_cooldown"] < 5000:
            self.field.create_text(500,135,text=str("3"), fill="yellow", font=self.ourFont2)
            parameters["start_check"] = False
        if parameters["gravity_cooldown"] >= 5000 and parameters["gravity_cooldown"] <= 5500:
            self.field.create_text(500,135,text = str("2"), fill="yellow", font=self.ourFont2)
        if parameters["gravity_cooldown"] > 5500 and parameters["gravity_cooldown"] < 6000:
            self.field.create_text(500,135,text = str("1"), fill="yellow", font=self.ourFont2)
        if parameters["gravity_cooldown"] <= 1000 and parameters["start_check"] == False:
            self.field.create_text(500,135,text=str("Reversing Gravity!"), fill="yellow", font=self.ourFont)
        
        #Animation for introductory prompt
        if parameters["begin"] == False and parameters["blink_time"] < 250:
            self.field.create_text(500,350,text=str("<Press any key to begin Testing>"), fill="yellow", font = self.ourFont)

        #Create ball
        self.field.create_image(parameters["ball_position"][0], parameters["ball_position"][1], image = self.ball_img)
       
        #Creating chaos cloud animation frame
        if parameters["cloud_exists"] == True:
            for i in parameters["cloud_points"]:
                self.field.create_oval(i[0]-random.randrange(30,parameters["cloud_size"]), i[1]-random.randrange(30,parameters["cloud_size"]), i[0]+random.randrange(30,parameters["cloud_size"]), i[1]+random.randrange(30,parameters["cloud.size"]), fill = "#476042", outline = "dark green")
        if parameters["cloud_exists"] == True and self.pong.cloud_cooldown <= 1000:
            self.field.create_text(500, 75, text = str("It's your old friend: Deadly Neurotoxin!"), fill = "yellow", font = self.ourFont)

        self.field.pack()

def play_music(filename, mode):
    """
    Loops background music.
    """
    while True:
        winsound.PlaySound(filename, mode)

try: 
	run_client()
	#Main series of operations necessary to prepare and run application.
	root = tk.Tk()
	#score = Score()
	app = GUI(master=root)
	app.master.title("Pongal")
	#app.pong.step()
	thread.start_new_thread(play_music, ("CaraMia.wav",winsound.SND_LOOP))
	app.mainloop()
	root.destroy()
except KeyboardInterrupt:
	sys.exit(1)