import socket
import sys
import threading
import json

host = 'localhost'
port = 50000
backlog = 5
size = 1024

commands = {}
parameters = {}

def run_server():
	global commands, parameters

	#Init server
	server = None
	try:
		server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		server.bind(('',port))
		print "Server started at: (%s,%s)" %(host,port)
		#server.listen(backlog)
	except socket.error, (value,errmessage):
		if server:
			server.close()
		print "Could not open socket: " + errmessage
		sys.exit(1)

	#Get the two client addresses
	addr1 = None
	addr2 = None
	(_,addr1)= server.recvfrom(size)
	while 1:
		(_,addr2) = server.recvfrom(size)
		if addr2!=addr1:
			break

	#Create game
	pong = Pong(server, addr1, addr2)

	#Begin receiving data
	t = threading.Thread(target = receiving, args = (server,))
	t.start()

	#Loop game and send parameters
	pong.step()

	#Send the info
	# while 1:
	# 	#Add to game model here
	# 	print message
	# 	#Send frame data to each client
	# 	server.sendto(json.dumps(message),addr1)
	# 	server.sendto(json.dumps(message),addr2)


def receiving(server):
	global commands
	while 1:
		data, addr = server.recvfrom(size)
		commands[repr(addr)] = data

#Class for creatting the two paddle objects use to interact with the game
class Paddle(object):
    def __init__(self, height, x_location, upbutton, downbutton):
        """
        Create a pong paddle with the given position and keybindings
        """
        self.initHeight = height
        self.height = self.initHeight
        self.x = x_location
        self.center = (self.x, (self.height - 50))
        self.KeyUp = upbutton
        self.KeyDown = downbutton
        self.move_up = False
        self.move_down = False

    def lower(self,event):
        """
        Given appropriate keypress, move paddle down.
        """
        self.move_down = True

    def lift(self,event):
        """
        Given appropriate keypress, move paddle up.
        """
        self.move_up = True

    def stop_left(self,event):
        """
        Stop left paddle when key is released.
        """
        if event.char == "w":
            self.move_up = False
        if event.char == "s":
            self.move_down = False

    def stop_right(self,event):
        """
        Stop stop right paddle when key is released (independent of character code).
        """
        self.move_up = False
        self.move_down = False

    def reset(self):
        """
        Reset position of paddles.
        """
        self.height = self.initHeight
        self.center = (self.x, (self.height - 50))

    def operate(self):
        """
        Runs all immediate paddle conditions.
        """
        if self.move_up == True and self.height >= 100:
            self.height -= 12

        if self.move_down == True and self.height <= 500:
            self.height += 12

        self.center = (self.x, self.height-50)
        
#Class that defines the properties of the game ball
class Ball(object):
    def __init__(self, position, size, speed):
        """
        Create a pong ball with the given position, size, and speed. 
        """
        self.initPosition = position 
        self.initSpeed = speed
        self.velocity = (0,0)
        self.size = size # Radius
        self.player = 0 # For score/new game reset

        self.reset()
        self.reset_mode = False

    def reflect(self, surface):
        """
        Alter the ball's velocity for a perfectly elastic
        collision with a surface defined by the unit normal surface.
        """
        diagonal = -2 * dot(surface, self.velocity)
        self.velocity = add(self.velocity, scale(surface, diagonal))

    def hitPaddle(self, surface):
        """
        Manage collision of ball and paddle. Reflect ball and increase
        ball's speed every 5 hits.
        """
        self.reflect(surface)
        self.hitTally+=1
        if self.hitTally>=5:
            self.speed+=.5
            self.velocity=(self.velocity[0]*self.speed/(self.speed-1),self.velocity[1]*self.speed/(self.speed-1))
            self.hitTally=0

    def hitPortal(self, exit):
        """
        Defines ball reaction to hitting a portal;
        Transfers ball position to center of specified exit portal.
        """
        self.position = pong.portals[exit].center

    def move(self):
        """
        Increment ball position, assuming no collisions.
        """
        self.position = add(self.position, self.velocity)

    def strike(self, player):
        """
        Set player to update score.
        """
        self.player = player
        self.reset()

    def accelerate(self, gravity):
        """
        Incrementally increase velocity of ball in reference direction.
        """
        adt = (gravity[0]*cos(gravity[1]), gravity[0]*sin(gravity[1]))
        self.velocity = add(self.velocity, adt)
        self.speed = (self.velocity[0]**2 + self.velocity[1]**2)**0.5

    def randomize_direction(self, polar_options):
        """
        Randomizes the direction of the ball's velocity vector within confines
        of specified angle range (to prevent lack of horizontal displacement when reset).
        """
        scalar = float(random.choice(polar_options))/100.
        angle = scalar*2*pi
        vx = self.speed*cos(angle)
        vy = self.speed*sin(angle)
        self.velocity = (vx, vy)

    def reset(self):
        """
        Handle game reset in case of goal or New Game button. Update scores and reset ball and paddle parameters.
        """
        score.update(self.player)
        self.position = self.initPosition
        self.speed = self.initSpeed
        polar_range = range(0, 13) + range(37, 63) + range(87,100)

        self.randomize_direction(polar_range)
        
        self.hitTally = 0
        self.player = 0

        self.reset_mode = True

#Class that controls the interaction between game and field elements
class Pong(object):
    def __init__(self,server,addr1, addr2):
        """
        Create a pong game. Create standard pong objects, events, and
        responses.
        """
        self.server = server
        self.addr1 = addr1
        self.addr2 = addr2
        self.game_time = 0
        self.restart_delay = 0
        self.blink_time = 0
        self.begin = False
        self.paddle_left = Paddle(300, 35, "w","s")
        self.paddle_right = Paddle(300, 965, "<Up>","<Down>")
        self.block1 = Block((0,0), 0, "purple")
        self.creature = Creature((0,0),0,"yellow",0.1)
        self.cloud = Cloud((0,0), 0, "dark green")
        self.ball = Ball((500,135), 10, .25)
        self.reset_game(None)

        # Game events: Ball hits [upper wall, lower wall, left paddle, right paddle, left wall, right wall]
        self.events = [lambda: commands["w"] > 0,
                        lambda: commands["w"] < 0,
                        lambda: commands["s"] > 0,
                        lambda: commands["s"] < 0,
                        lambda: commands["up"] > 0,
                        lambda: commands["up"] < 0,
                        lambda: commands["down"] > 0,
                        lambda: commands["down"] < 0
                        lambda: commands["a"] > 0,
                        lambda: commands["d"] > 0,
                        lambda: commands["a"] < 0,
                        lambda: commands["d"] < 0,
                        lambda: commands["left"] > 0,
                        lambda: commands["right"] > 0,
                        lambda: commands["left"] < 0,
                        lambda: commands["right"] < 0,
                        lambda: commands["any"] > 0,
                        lambda: commands["Quit"] > 0,
                        lambda: commands["Restart"] > 0,
                        # end of control inputs, start of model conditions
                        lambda: self.ball.position[1] < 10 and self.ball.velocity[1] < 0,
                        lambda: self.ball.position[1] > 490 and self.ball.velocity[1] > 0,
                        self.hits_left_paddle,                  #Hit paddle: portal check
                        self.hits_right_paddle,                 #Hit paddle: portal check
                        self.hits_left_paddle,                  #Hits paddle: normal
                        self.hits_right_paddle,                 #Hits paddle: normal
                        lambda: self.ball.position[0] > 995,    #Score on right edge
                        lambda: self.ball.position[0] < 5,      #Score on left edge
                        self.hits_portal,                       #Hits portal
                        lambda: self.portal_cooldown >= 6000,   #Time: Randomize portal
                        lambda: self.gravity_cooldown >= 6000,  #Time: Randomize gravity
                        lambda: self.block_cooldown >= 3000,    #Time: Randomize block
                        self.hits_block,                        #Hits block
                        lambda: self.creature_cooldown >= 8000, #Time: Randomize creature
                        lambda: self.creature.center[1] < 10 and self.creature.velocity[1] < 0,
                        lambda: self.creature.center[1] > 490 and self.creature.velocity[1] > 0,
                        self.hits_creature,                     #Hits creature
                        lambda: self.cloud_cooldown >= 10000,   #Time: Randomize cloud
                        self.enters_cloud,                      #Hits cloud
                        lambda: self.cloud_cooldown >= 3000,    #Time: Kill cloud
                        lambda: self.game_over == True]


        # Game responses: [bounce back in appropriate direction x4, score ball x2]
        self.responses = [self.paddle_left.lift,
                        self.paddle_left.stop_left,
                        self.paddle_left.lower,
                        self.paddle_left.stop_left,
                        self.paddle_right.lift,
                        self.paddle_right.stop_right,
                        self.paddle_right.lower,
                        self.paddle_right.stop_right,
                        lambda: self.enable_left_warp(0),
                        lambda: self.enable_left_warp(1),
                        self.disable_left_warp,
                        self.disable_left_warp,
                        lambda: self.enable_right_warp(0),
                        lambda: self.enable_left_warp(1),
                        self.disable_right_warp,
                        self.disable_right_warp,
                        self.begin_game,
                        self.end_game,
                        self.new_game,
                        # end of controlled commands, start of model responses
                        lambda: self.ball.reflect((0,-1)),
                        lambda: self.ball.reflect((0,1)),
                        lambda: self.warp("left"),              #Hit paddle: portal
                        lambda: self.warp("right"),             #Hit paddle: portal
                        lambda: self.ball.hitPaddle((1,0)),     #Hits paddle: normal
                        lambda: self.ball.hitPaddle((-1,0)),    #Hits paddle: normal
                        lambda: self.reset_game(1),             #Score on right edge
                        lambda: self.reset_game(2),             #Score on left edge
                        self.teleport,                          #Hits portal
                        self.randomize_portals,                 #Time: Randomize portal
                        self.change_gravity,                    #Time: Randomize gravity
                        self.generate_blocks,                   #Time: Randomize block
                        self.block_bounce,                      #Hits block
                        self.generate_creature,                 #Time: Randomize creature
                        lambda: self.creature.reflect((0,-1)),
                        lambda: self.creature.reflect((0,1)),
                        self.creature_bounce,                   #Hits creature
                        self.generate_cloud,                    #Time: Randomize cloud
                        self.unleash_chaos,                     #Hits cloud
                        self.cloud.destroy,                     #Time: Kill cloud
                        lambda:self.reset_game(0)]

#Game run methods
    def begin_game(self, event):
        """
        Detects conditional when first game per session is played, such
        that a single start prompt can be instituted.
        """
        if self.begin == False:
            self.begin = True
            self.restart_delay = 0

    def new_game(self):
        """
        Used to force the game into resetting.
        """
        self.game_over = True

    def pack_parameters(self):
        global parameters
        parameters["restart_delay"]            = self.restart_delay
        parameters["begin"]                    = self.begin
        parameters["lwarp_enable"]             = self.lwarp_enable
        parameters["lwarp"]                    = self.lwarp
        parameters["rwarp_enable"]             = self.rwarp_enable
        parameters["rwarp"]                    = self.rwarp
        parameters["paddle_left_center"]       = self.paddle_left.center
        parameters["paddle_right_center"]      = self.paddle_right.center
        parameters["paddle_left_height"]       = self.paddle_left.height
        parameters["paddle_right_height"]      = self.paddle_right.height
        parameters["cyan_portal_override"]     = self.cyan_portal.override
        parameters["cyan_portal_space"]        = self.cyan_portal.space
        parameters["cyan_portal_color_in"]     = self.cyan_portal.color_in
        parameters["cyan_portal_fill_color"]   = self.cyan_portal.fill_color
        parameters["orange_portal_override"]   = self.orange_portal.override
        parameters["orange_portal_space"]      = self.orange_portal.space
        parameters["orange_portal_color_in"]   = self.orange_portal.color_in
        parameters["orange_portal_fill_color"] = self.orange_portal.fill_color
        parameters["blue_portal_override"]     = self.blue_portal.override
        parameters["blue_portal_space"]        = self.blue_portal.space
        parameters["blue_portal_color_in"]     = self.blue_portal.color_in
        parameters["blue_portal_fill_color"]   = self.blue_portal.fill_color
        parameters["red_portal_override"]      = self.red_portal.override
        parameters["red_portal_space"]         = self.red_portal.space
        parameters["red_portal_color_in"]      = self.red_portal.color_in
        parameters["red_portal_fill_color"]    = self.red_portal.fill_color 
        parameters["block1_center"]            = self.block1.center
        parameters["block1_state"]             = self.block1.state
        parameters["creature_center"]          = self.creature.center
        parameters["creature_state"]           = self.creature.state
        parameters["gravity_cooldown"]         = self.gravity_cooldown
        parameters["start_check"]              = self.start_check
        parameters["begin"]                    = self.begin
        parameters["blink_time"]               = self.blink_time
        parameters["ball_position"]            = self.ball.position
        parameters["cloud_exists"]             = self.cloud.exists
        parameters["cloud_points"]             = self.cloud.points
        parameters["cloud_size"]               = self.cloud.size
        parameters["cloud_cooldown"]           = self.cloud_cooldown
        parameters["scoreP1"]                  = score.scoreP1
        parameters["scoreP2"]                  = score.scoreP2

    def step(self):
        """
        Calculate the next game state.
        """
        if self.ball.reset_mode == True:
            self.restart_delay = 0
            self.ball.reset_mode = False

        self.paddle_right.operate()
        self.paddle_left.operate()
        if self.begin == True and self.restart_delay > 1000:
            self.ball.accelerate((self.gravity,(pi/2)))
            self.ball.move()
            self.portal_cooldown += 5
            self.portal_cooldown += 5
            self.gravity_cooldown += 5
            self.block_cooldown += 5
            self.creature_cooldown +=5
            self.creature.move()
            self.cloud_cooldown += 5
            self.chaos_cooldown += 5

        if self.restart_delay <= 1000:
            self.restart_delay += 5

        # Check for events
        self.blink_time+= 5
        if self.blink_time >= 500:
            self.blink_time = 0

        else:
            for event, response in zip(self.events, self.responses):
                if event():
                    response()
                     
                    #Send data to client
                    self.pack_parameters()
                    self.server.sendto(json.dumps(parameters),self.addr1)
                    self.server.sendto(json.dumps(parameters),self.addr2)
                    
        # Run pong game with time step size of 0.005 seconds
        after(5, self.step)
        self.game_time += 5

#Events
    def hits_left_paddle(self):
        """
        Detects the entrance of the ball's position into the left paddle's space.
        """
        if self.ball.velocity[0]<0:
            return self.ball.position[0] < 50 and self.ball.position[1] <= self.paddle_left.height+10 and self.ball.position[1] >= self.paddle_left.height-110

    def hits_right_paddle(self):
        """
        Detects the entrance of the ball's position into the right paddle's space.
        """

        if self.ball.velocity[0]>0:
            return self.ball.position[0] > 950 and self.ball.position[1] <= self.paddle_right.height+10 and self.ball.position[1] >= self.paddle_right.height-110

    def hits_portal(self):
        """
        Detects the entrance of the ball's position into any active portal space 
        (only when the respective portal's paddle override is not active).
        """
        for i in self.portals:
            if self.ball.position[0] < self.portals[i].space[1][0] and self.ball.position[0] > self.portals[i].space[0][0] and self.ball.position[1] > (self.portals[i].center[1]-10) and self.ball.position[1] < (self.portals[i].center[1]+10):
                self.catcher = self.portals[i].color_in
                if self.catcher != self.lwarp and self.catcher != self.rwarp:
                    return True   
    
    def hits_block(self):
        """
        Detects the entrance of the ball's position into the block space.
        """
        return self.ball.position[0] > self.block1.space[0][0] and self.ball.position[0] < self.block1.space[1][0] and self.ball.position[1] < self.block1.space[1][1] and self.ball.position[1] > self.block1.space[0][1]

    def hits_creature(self):
        """
        Detects the entrance of the ball's position into the creature space.
        """
        return self.ball.position[0] > self.creature.space[0][0] and self.ball.position[0] < self.creature.space[1][0] and self.ball.position[1] < self.creature.space[1][1] and self.ball.position[1] > self.creature.space[0][1]


    def enters_cloud(self):
        """
        Detects the entrance of the ball's position into the cloud space.
        """
        return self.ball.position[0] > self.cloud.space[0][0] and self.ball.position[0] < self.cloud.space[1][0] and self.ball.position[1] < self.cloud.space[1][1] and self.ball.position[1] > self.cloud.space[0][1]
    
    def enable_left_warp(self, event):
        """
        Controls the individual paddle overrides of the left player's portals.
        """
        self.lwarp_enable = True
        if event == 0:
            self.lwarp = "orange"
            self.cyan_portal.override = True
        elif event == 1:
            self.lwarp = "cyan"
            self.orange_portal.override = True

    def disable_left_warp(self, event):
        """
        Endes the override state of all left-player portals when key is released.
        """
        self.lwarp_enable = False
        self.lwarp = None
        self.orange_portal.override = False
        self.cyan_portal.override = False

    def enable_right_warp(self, event):
        """
        Controls the individual paddle overrides of the right player's portals.
        """
        self.rwarp_enable = True
        self.override = True
        if event == 0:
            self.rwarp = "red"
            self.blue_portal.override = True
        elif event == 1:
            self.rwarp = "blue"
            self.red_portal.override = True

    def disable_right_warp(self, event):
        """
        Ends the override state of all right-player portals when key is released.
        """
        self.rwarp_enable = False
        self.rwarp = None
        self.red_portal.override = False
        self.blue_portal.override = False

#Responses
    def warp(self, side):
        """
        Overrides the current state of a selected portal and relocates it
        at the position and orientation of a referenced paddle.
        """
        if self.portal_cooldown >= 100:
            self.ball.hitTally +=1
            if side == "left" and self.lwarp_enable == True: # Combine these at some point
                if self.portals[self.lwarp].center[1] == 10:
                    theta_added = (3*pi/2)
                elif self.portals[self.lwarp].center[1] == 490:
                    theta_added = (pi/2)
                self.portal_cooldown = 0
                self.ball.position = self.portals[self.portals[self.lwarp].color_out].center
                theta_in = arctan(self.ball.velocity[1]/self.ball.velocity[0])       
                self.ball.velocity = (self.ball.speed*cos(theta_added+theta_in), (self.ball.speed*sin(theta_added+theta_in)))
            elif side == "right" and self.rwarp_enable == True:
                if self.portals[self.rwarp].center[1] == 10:
                    theta_added = (3*pi/2)
                elif self.portals[self.rwarp].center[1] == 490:
                    theta_added = (pi/2)
                self.portal_cooldown = 0
                self.ball.position = self.portals[self.portals[self.rwarp].color_out].center
                theta_in = arctan(self.ball.velocity[1]/self.ball.velocity[0])
                self.ball.velocity = (self.ball.speed*cos(theta_added+theta_in), (self.ball.speed*sin(theta_added+theta_in)))
    
    def reset_game(self, player):
        """
        Resets the state of all game components, cooldowns, and operators
        in preparation for a new match.
        """
        self.ball.strike(player)
        self.block1.destroy()
        self.creature.destroy()
        self.cloud.destroy()

        self.randomize_portals()
        self.catcher = None
        self.portal_cooldown = 300
        self.portal_cooldown = 0
        self.gravity_cooldown = 0
        self.block_cooldown = 0
        self.creature_cooldown = 0
        self.cloud_cooldown = 0
        self.chaos_cooldown = 0
        self.gravity = 0.1
        self.start_check = True
        self.lwarp_enable = False
        self.rwarp_enable = False
        self.lwarp = None
        self.rwarp = None
        self.game_over = False
      
    def teleport(self):
        """
        Relocates the ball at a portal position which corresponds to the detected entrance,
        and redistributes velocity components according to the new momentum direction.
        """
        if self.portal_cooldown >= 100:
            if self.portals[self.catcher].override == True:
                theta_in = arctan(self.ball.velocity[1]/self.ball.velocity[0])
                if self.catcher == "cyan" or self.catcher == "orange":
                    self.ball.position = self.paddle_left.center
                    theta_out = sign(theta_in)*((pi/2)-abs(theta_in))
                elif self.catcher == "red" or self.catcher == "blue":
                    self.ball.position = self.paddle_right.center
                    theta_out = sign(theta_in)*((pi/2)-abs(theta_in))+pi
                self.ball.velocity = (self.ball.speed*cos(theta_out), (self.ball.speed*sin(-theta_out)))
            else:
                self.ball.position = self.portals[self.portals[self.catcher].color_out].center
                if self.portals[self.catcher].center[1] == self.portals[self.portals[self.catcher].color_out].center[1]:
                    self.ball.velocity = (-self.ball.velocity[0], -self.ball.velocity[1])
            self.portal_cooldown = 0

    def randomize_portals(self):
        """
        Defines/Changes the wall-position of all portals randomly.
        """
        y_positions = [10,490]
        x_positions = range(100,901)
        resulting_positions = []

        for portal in range(4):
            selection = random.choice(x_positions)
            resulting_positions.append(selection)
            x_positions.remove(selection)

            for i in range(1,121):
                if (selection-i) in x_positions:
                    x_positions.remove(selection-i)
                if (selection+i) in x_positions:
                    x_positions.remove(selection+i)

        self.orange_portal = Portal("orange", "cyan", (resulting_positions[0], random.choice(y_positions)))
        self.cyan_portal = Portal("cyan", "orange", (resulting_positions[1], random.choice(y_positions)))
        self.red_portal = Portal("red", "blue", (resulting_positions[2], random.choice(y_positions)))
        self.blue_portal = Portal("blue", "red", (resulting_positions[3], random.choice(y_positions)))
        self.portals = {"orange":self.orange_portal, "cyan":self.cyan_portal, "red":self.red_portal, "blue":self.blue_portal}
        self.portal_cooldown = 0
        self.start_check = False
    
    def change_gravity(self):
        """
        Changes the direction in which the gravity field acts.
        """
        self.gravity = -self.gravity
        self.gravity_cooldown = 0
    
    def generate_blocks(self):
        """
        Introduces a breakout block to the field.
        """
        if self.block1.exists == False:
            self.block1.generate(35)
            self.block1.exists = True
            self.block_cooldown = 0

    def block_bounce(self):
        """
        Dictates reaction of ball when hitting a block.
        """
        edge = self.block1.breakout(self.ball.position[0], self.ball.position[1])
        self.ball.reflect(edge)
        self.block_cooldown = 0

    def generate_creature(self):
        """
        Introduces a creature to the field.
        """
        self.creature.generate(35)
        self.creature.exists = True
        self.creature_cooldown = 0

    def creature_bounce(self):
        """
        Dictates reaction of ball when hitting a creature.
        """
        if self.creature.check_collision():
            edge = self.creature.breakout(self.ball.position[0],self.ball.position[1])
            self.ball.reflect(edge)

    def generate_cloud(self):
        """
        Introduces a cloud space to the field."
        """
        self.cloud.exists = True
        self.cloud.generate(75)
        self.cloud.generate_points()
        self.cloud_cooldown = 0

    def unleash_chaos(self):
        """
        Randomizes ball direction (when entering coud).
        """
        if self.chaos_cooldown >= 100:
            self.ball.randomize_direction(range(100))
            self.chaos_cooldown = 0

#Class to create the object which stores information about game results
class Score(object):
    def __init__(self):
        """
        Create 2-player score keeping object.
        """
        self.scoreP1 = 0
        self.scoreP2 = 0

    def update(self, player):
        """
        Increment a player's score.
        """
        if player == 1:
            self.scoreP1 += 1
        elif player == 2:
            self.scoreP2 += 1
        elif player == 0:
            self.scoreP1 = 0
            self.scoreP2 = 0

#Class which defines the properties of each portal
class Portal(object):
    """
    Interactive object that carries information about the color and positions
    of itself and another "linked" mate.
    """
    def __init__(self, color_in, color_out, center):
        self.override = False
        self.color_in = color_in # color property of portal
        self.color_out = color_out # indicates portal which serves as exit
        self.fill_color = "gray11" # overlay for aesthetic purposes
        self.center = center
        self.space = ((self.center[0]-50, self.center[1]+5),(self.center[0]+50, self.center[1]-5))

#Class which is inherited by game obstacles which interfere with the ball.
class Obstacle(object):
    """
    Foundation for all forms of game obstacles, storing information about position,
    space, and state.
    """
    def __init__(self, coordinates, dimension, color):
        self.center = coordinates
        self.color = color
        self.size = dimension
        self.update_space()
        self.exists = False
        self.state = "hidden"

    def update_space(self):
        """
        Defines the space surrounding the center of the object in which interaction occurs.
        """
        self.space = (((self.center[0] - self.size), (self.center[1] - self.size)), ((self.center[0] + self.size), (self.center[1] + self.size)))

    def generate(self, dimension):
        """
        Introduces the obstacle to the field.
        """
        self.center = (random.randrange(100,900),random.randrange(100,400))
        self.size = dimension
        self.update_space()
        self.state = "normal"

    def destroy(self):
        """
        Removes the obstacle from the field.
        """
        self.center = (0,0)
        self.size = 0
        self.update_space()
        self.exists = False
        self.state = "hidden"

#Class which defines the unique properties of the breakout block obstacle.
class Block(Obstacle):
    """
    Breakout block which redirects the ball.
    """
    def breakout(self, ball_x, ball_y):
        """
        Reflects ball based on which side is hit, then destroys self.
        """
        x_contact = abs(ball_x - self.center[0])
        y_contact = abs(ball_y - self.center[1])
        theta_contact = arctan(y_contact / x_contact)
        self.destroy()
        self.exists = False
        if theta_contact > 0.7854:
            return (0 , 1)
        else:
            return (1 , 0)

#Class which defines the unique properties of the chaos cloud obstacle.
class Cloud(Obstacle):
    """
    Chaos cloud which randomizes ball direction.
    """
    def generate_points(self):
        """
        creates 20 random points which serve as centers of circles with
        randomly fluctuating radii, providing animated indication of position.
        """
        self.points = []
        low_x = self.center[0] - self.size
        high_x = self.center[0] + self.size
        low_y = self.center[1] - self.size
        high_y = self.center[1] + self.size
        for i in range(20):
            random_x = random.randrange(low_x, high_x)
            random_y = random.randrange(low_y, high_y)
            self.points.append((random_x, random_y))

#Class which defines the unique properties of the creature.
class Creature(Obstacle):
    """
    Moving obstacle which redirects ball.
    """
    def __init__(self, coordinates, dimension, color,speed):
        Obstacle.__init__(self, coordinates, dimension, color)
        self.velocity = (0,speed)
        self.last_contact = 0

    def breakout(self, ball_x, ball_y):
        """
        Reflects the ball based on which side is hit.
        """
        self.last_contact = 0
        x_contact = abs(ball_x - self.center[1])
        y_contact = abs(ball_y - self.center[0])
        theta_contact = arctan(y_contact / x_contact)
        if theta_contact > 0.7854:
            return (0 , 1)
        else:
            return (1 , 0)

    def check_collision(self):
        """
        Checks for same object multi-collisions.
        """
        return self.last_contact>10

    def move(self):
        """
        Increment Creature's position assuming no collisions.
        """
        self.center = add(self.center,self.velocity)
        self.update_space()
        self.last_contact += 1

    def reflect(self, surface):
        """
        Alter the creature's velocity for a perfectly elastic
        collision with a surface defined by the unit normal surface.
        """
        diagonal = -2 * dot(surface, self.velocity)
        self.velocity = add(self.velocity, scale(surface, diagonal))
        
#Series of mathematical functions used to manage vectorial operations.
def dot(x, y):
    """
    2D dot product
    """
    return x[0]*y[0] + x[1]*y[1]

def scale(x, a):
    """
    2D scalar multiplication
    """
    return (x[0]*a, x[1]*a)

def add(x, y):
    """
    2D vector addition
    """
    return (x[0]+y[0], x[1]+y[1])

def sign(x):
    """
    determines the sign of a given value
    """
    return math.copysign(1, x)

try:
	run_server()
except KeyboardInterrupt:
	sys.exit(1)

